using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This class handles all the functionality internal to instantiated AudioEmitters
public class AudioEmitter : MonoBehaviour
{
    private AudioSource audioSource;
    public List<AudioClip> audioClips;

    //Debug panel properties and colours
    [SerializeField] GameObject debugPanel;
    [SerializeField] RectTransform debugPanelOutline;
    [SerializeField] RectTransform debugPanelbackground;
    [SerializeField] Text audioRegionName;
    [SerializeField] Text audioClipName;
    [SerializeField] Text volumeText;
    [SerializeField] Text pitchText;
    private Color playingColour = new Vector4(0.8f, 0.8f, 0.8f, 1.0f);
    private Color defaultColour = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
    private Color disabledColour = new Vector4(0.3f, 0.3f, 0.3f, 1.0f);

    //Region-specific properties
    private float pitchVariation;
    private int minimumSpacing;
    private int maximumSpacing;

    private AudioListener audioListener;
    private bool emitterDisabled = false;
    private bool playing = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        debugPanel.SetActive(false);
        audioListener = FindObjectOfType<AudioListener>();
    }

    void Update()
    {     
        //Billboard the transform (mainly for viewing the debug panels)
        transform.LookAt(Camera.main.transform);
        
        //Check for distance from audio main Audio Listener; if the AudioEmitter is out of range, disable it
        Vector3 distanceFromCamera = transform.position - audioListener.transform.position;

        if (Vector3.Magnitude(distanceFromCamera) > 40.0f)
        {
            emitterDisabled = true;
            audioSource.enabled = false;
            playing = false;
            SetPanelBackgroundColour(disabledColour);
            Debug.DrawLine(transform.position, audioListener.transform.position, Color.red);
        }
        else
        {
            emitterDisabled = false;
            audioSource.enabled = true;
            SetPanelBackgroundColour(defaultColour);
            Debug.DrawLine(transform.position, audioListener.transform.position, Color.green);
        }
    }

    //Audio playback functionality is housed in LateUpdate so that AudioEmitters out of range of 
    //the AudioListener don't attempt to use a disabled audioSource
    private void LateUpdate()
    {
        if (!audioSource.isPlaying && playing == false && !emitterDisabled)
        {
            playing = true;
            StartCoroutine(PlaySound());
            int randomNumber = Random.Range(0, audioClips.Count);   //minInclusive; maxExclusive
            audioSource.clip = audioClips[randomNumber];
            audioSource.pitch = Random.Range(1 - pitchVariation, 1 + pitchVariation);
            pitchText.GetComponent<Text>().text = audioSource.pitch.ToString();
        }

        if (audioSource.isPlaying && !emitterDisabled)
        {
            SetPanelBackgroundColour(playingColour);
            audioClipName.GetComponent<Text>().text = audioSource.clip.name;

        }

        if (!audioSource.isPlaying && !emitterDisabled)
        {
            SetPanelBackgroundColour(defaultColour);
        }
    }

    //Plays the audioSource after a variable delay
    IEnumerator PlaySound()
    {
        yield return new WaitForSeconds(Random.Range(minimumSpacing, maximumSpacing));
        //Debug.Log("Play!");

        if (!emitterDisabled)
        {
            audioSource.Play();
            playing = false;
        }
    }

    //Adds audioClips to this AudioEmitter's list
    public void AddAudioClips(AudioClip[] audioClips)
    {
        for (int i = 0; i < audioClips.Length; i++)
        {
            this.audioClips.Add(audioClips[i]);
        }       
    }

    //Setters used for passing on region-specific playback and debug properties to an AudioEmitter
    public void SetVolume(float volume)
    {
        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.volume = volume;
    }

    public void SetPitchVariation(float pitchVariation)
    {
        this.pitchVariation = pitchVariation;
    }

    public void SetMinimumSpacing(int minimumSpacing)
    {
        this.minimumSpacing = minimumSpacing;
    }

    public void SetMaximumSpacing(int maximumSpacing)
    {
        this.maximumSpacing = maximumSpacing;
    }

    public void SetPanelOutlineColour(Color colour)
    {
        debugPanelOutline.GetComponent<Image>().color = colour;
    }

    public void SetPanelBackgroundColour(Color colour)
    {
        debugPanelbackground.GetComponent<Image>().color = colour;
    }

    public void SetPanelText(string text)
    {
        audioRegionName.GetComponent<Text>().text = text;
    }

    public void SetPanelVolumeText(string text)
    {
        volumeText.GetComponent<Text>().text = text;
    }

    public void SetDebugPanelVisible()
    {
        debugPanel.SetActive(true);
    }

    public void SetDebugPanelInvisible()
    {
        debugPanel.SetActive(false);
    }
}