using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class is responsible for instantiating AudioEmitter prefabs at the correct locations on the 3D
//plane, using coordinates from the Perlin noise texture.
public class SoundscapeCreator : MonoBehaviour
{
    private MapGenerator mapGenerator;
    private Texture2D proceduralTexture;
    [SerializeField] private GameObject audioEmitter;                   //AudioEmitter prefab
    private MeshFilter planeMesh;                                       //the plane onto which the noise texture is projected
    private int seed;
    private int textureScale;
    private List<GameObject> audioEmitters = new List<GameObject>();    //stores all AudioEmitters in the scene
    private bool emitterDebugView = false;                              //toggle for debugging AudioEmitters in game
    //[SerializeField] private AudioClip clip1;

    private SoundType[] regions;

    private void OnEnable()
    {
        mapGenerator = gameObject.GetComponent<MapGenerator>();
        seed = mapGenerator.GetSeed();  
        textureScale = GetComponent<MapDisplay>().GetTextureScale();
        regions = GetComponent<MapGenerator>().regions;
    }

    void Start()
    {
        proceduralTexture = (Texture2D)GetComponentInChildren<MeshRenderer>().material.mainTexture;
        GenerateSoundscape();
    }

    void Update()
    {
        //Toggles the AudioEmitter debug planels in game
        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            ToggleDebugView();
        }
    }

    //
    public void GenerateSoundscape()
    {
        //Loop through the noise texture and, for each position that is randomly assigned an AudioEmitter,
        //instantiate an AudioEmitter prefab with properties according to it's region (defined in MapGenerator class).
        for (int x = 0; x < proceduralTexture.width; x++)
        {
            for (int y = 0; y < proceduralTexture.height; y++)
            {
                bool containsEmmitter = GetRandomBool(x, y, seed);               
                if (containsEmmitter)
                {
                    //Create a new emitter and add it to the soundscape's emitter list
                    GameObject audioEmitterObject = Instantiate(audioEmitter, GetPositionOnPlane(x, y), Quaternion.identity) as GameObject;
                    audioEmitters.Add(audioEmitterObject);

                    AudioEmitter audioEmitterScript = audioEmitterObject.GetComponent<AudioEmitter>();
                    audioEmitterObject.transform.SetParent(this.transform, false);
                    audioEmitterObject.transform.position = GetPositionOnPlane(x, y);
                    AudioSource audioSource = audioEmitterObject.GetComponent<AudioSource>();
                    audioSource.playOnAwake = false;
                    audioSource.spatialBlend = 1.0f;
                    Color colour = proceduralTexture.GetPixel(x, y);

                    //Loop through the regions in the MapGenertor
                    for (int i = 0; i < regions.Length; i++)
                    {
                        if (CheckColourMatch(colour, regions[i].colour))
                        {
                            //Add audio clips to the emitter's audioClips list, as well as region-specific properties
                            audioEmitterScript.AddAudioClips(regions[i].audioClips);
                            audioEmitterScript.SetVolume(regions[i].volume);
                            audioEmitterScript.SetPitchVariation(regions[i].pitchVariation);
                            audioEmitterScript.SetMinimumSpacing(regions[i].minimumSpacing);
                            audioEmitterScript.SetMaximumSpacing(regions[i].maximumSpacing);
                            audioEmitterScript.SetPanelOutlineColour(regions[i].colour);
                            audioEmitterScript.SetPanelText(regions[i].name);
                            audioEmitterScript.SetPanelVolumeText((regions[i].volume).ToString());
                            break;
                        }   
                    }
                }
            }
        }
    }

    //Determines whether or not a position on the Perlin noise texture will be assigned
    //an audio emitter, based on a random seed.
    private bool GetRandomBool(int numberOne, int numberTwo, int seed)
    {
        int modifiedSeed = seed + (numberOne + 1) * numberTwo;
        UnityEngine.Random.InitState(modifiedSeed);
        int randomNumber = Random.Range(0, 91);
        return randomNumber <= 2;
    }

    //Returns the position on the plane mesh matching a given pixel in the prodecural texture
    //NOTE: An error in my implementation here prevents audio emitters spawning correctly on anything
    //but a 40 x 40 texture size.
    private Vector3 GetPositionOnPlane(int x, int y)
    {
        Vector3 position = new Vector3();
        //Get the plane mesh
        planeMesh = GetComponentInChildren<MeshFilter>();
        Renderer planeMeshRenderer = planeMesh.GetComponent<Renderer>();
        //Get the max point in world space; this corresponds to the lower-left corner of the procedural texture
        Vector3 planeMax = planeMeshRenderer.bounds.max;
        float offset = (planeMesh.transform.localScale.x / textureScale) / 2;
        position.x = planeMax.x - (x * (planeMesh.transform.localScale.x / textureScale)) - offset;
        position.z = planeMax.z - (y * (planeMesh.transform.localScale.z / textureScale)) - offset;
        position.y = planeMeshRenderer.transform.position.y + 1.0f;
        return position;
    }

    //Compares two colours and returns whether they are closely matching
    private bool CheckColourMatch(Color colour1, Color colour2)
    {
        float threshold = 0.01f;
        return (Mathf.Abs(colour1.r - colour2.r) < threshold && Mathf.Abs(colour1.g - colour2.g) < threshold && Mathf.Abs(colour1.b - colour2.b) < threshold);
    }

    //Shows in-game debug panels for each AudioEmitter (showing Region name, AudioClip played, volume and pitch settings)
    public void ToggleDebugView()
    {
        if (!emitterDebugView)
        {
            for (int i = 0; i < audioEmitters.Count; i++)
            {
                audioEmitters[i].GetComponent<AudioEmitter>().SetDebugPanelVisible();
            }
            emitterDebugView = true;
        }
        else
        {
            for (int i = 0; i < audioEmitters.Count; i++)
            {
                audioEmitters[i].GetComponent<AudioEmitter>().SetDebugPanelInvisible();
            }
            emitterDebugView = false;
        }
    }
}