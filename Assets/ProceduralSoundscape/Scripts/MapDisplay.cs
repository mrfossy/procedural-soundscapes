using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Displays the coloured Perlin noisemap
public class MapDisplay : MonoBehaviour
{
    public Renderer textureRenderer;
    private int textureScale = 4;

    public void DrawTexture(Texture2D texture)
    {
        textureRenderer.enabled = true;
        //Apply texture to texture renderer
        //sharedMaterial allows you to update the texture in the editor (unlike .material)
        textureRenderer.sharedMaterial.mainTexture = texture;
        //Set the size of the plane to the same size of the texture map; scaled down to get a higher grid resolution
        textureRenderer.transform.localScale = new Vector3(texture.width / textureScale, 1, texture.height / textureScale);
    }

    public void SetInvisible()
    {
        //textureRenderer.enabled = false;
        MeshRenderer meshRenderer = GetComponentInChildren<MeshRenderer>();
        meshRenderer.enabled = false;
    }

    public int GetTextureScale()
    {
        return textureScale;
    }
}
