using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Handles the texture creation code
public static class TextureGenerator
{
    //Generates a coloured Perlin noise texture
    public static Texture2D TextureFromColourMap(Color[] colourMap, int width, int height)
    {
        Texture2D texture = new Texture2D(width, height);
        //Remove texture blur and texture wrapping
        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        texture.SetPixels(colourMap);
        texture.Apply();
        return texture;
    }

    //Generates a normal (i.e. greyscale) Perlin noise texture
    public static Texture2D TextureFromHeightMap(float[,] heightMap)
    {
        //Get width and height of noise map
        int width = heightMap.GetLength(0);
        int height = heightMap.GetLength(1);

        //Create a 1D array of all the colours that will be displayed on the texture
        Color[] colourMap = new Color[width * height];

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                //Set the colour map (which is a 1D array) somewhere between black and white (depending on noise map value)
                colourMap[y * width + x] = Color.Lerp(Color.black, Color.white, heightMap[x, y]);
            }
        }

        return TextureFromColourMap(colourMap, width, height);
    }
}