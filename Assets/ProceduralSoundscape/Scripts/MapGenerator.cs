using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class is responsible for the higher-level procedural generation functionality. I have also used this class
//to house all properties associated with the different 'soundscape regions' in my system, as well as for passing
//per-region audio properties on to the SoundscapeCreator class.

//The procedural generation code came from the first four parts of a procedural generation YouTube tutorial by Sebastian Lague:
//https://www.youtube.com/watch?v=wbpMiKiSKm8&list=RDCMUCmtyQOKKmrMVaKuRXz02jbQ&index=4
public class MapGenerator : MonoBehaviour
{
    public enum DrawMode{ColourMap, Invisible };
    //public enum DrawMode{ColourMap, NoiseMap, Invisible };
    public DrawMode drawMode;

    //Texture size should be customizable, but an error in my implementation prevents this
    private int mapWidth = 40;   
    private int mapHeight = 40;

    //Perlin noise properties
    [SerializeField] private float noiseScale;
    [SerializeField] private int octaves;
    [Range(0,1)]
    [SerializeField] private float persistence;
    [SerializeField] private float lacunarity;
    [SerializeField] private int seed;
    [SerializeField] private Vector2 offset;

    //Allows for updating of texture map whenever a value is changed; public so it is accessible from MapGeneratorEditor class
    public bool autoUpdate;

    public SoundType[] regions;

    private void OnEnable()
    {
        //Ensures map remains visible on entering play mode
        GenerateMap();
    }

    //Generates the Perlin noise map
    public void GenerateMap()
    {
        float[,] noiseMap = PerlinNoise.GenerateNoiseMap(mapWidth, mapHeight, seed, noiseScale, octaves, persistence, lacunarity, offset);

        Color[] colourMap = new Color[mapWidth * mapHeight];

        //Loop through the noise map
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                //Set height at this current point to current noise map coordinate
                float currentHeight = noiseMap[x, y];

                //Loop through all ofthe regions
                for (int i = 0; i < regions.Length; i++)
                {
                    if (currentHeight <= regions[i].height)
                    {
                        colourMap[y * mapWidth + x] = regions[i].colour;
                        break;
                    }
                }
            }
        }

        //Get reference to MapDisplay and call the draw function
        MapDisplay display = FindObjectOfType<MapDisplay>();
        
/*        if (drawMode == DrawMode.NoiseMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
        }*/
        if (drawMode == DrawMode.ColourMap)
        {
            display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
        }
        else if (drawMode == DrawMode.Invisible)
        {
            //Draw the coloured texture and set it to invisible
            display.DrawTexture(TextureGenerator.TextureFromColourMap(colourMap, mapWidth, mapHeight));
            display.SetInvisible();
        }
    }

    //OnValidate is called automatically whenever a script variable is changed in inspector
    //Used here for clamping variables
    private void OnValidate()
    {
        //Clamp the map width and height
        if (mapWidth < 1)
        {
            mapWidth = 1;
        }

        if (mapHeight < 1)
        {
            mapHeight = 1;
        }

        if (lacunarity < 1)
        {
            lacunarity = 1;
        }

        if (octaves < 0)
        {
            octaves = 0;
        }

        //Ensures map remains visible after exiting play mode
        //GenerateMap();
    }

    public int GetSeed()
    {
        return seed;
    }
}

//System.Serializable means this will show up in the inspector
[System.Serializable]
public struct SoundType
{
    public string name;
    public float height;    //height value of this type of terrain
    public Color colour;
    public AudioClip[] audioClips;  //stores audio clips for this region
    [Range(0,1)]
    public float volume;
    [Range(0, 1)]
    public float pitchVariation;
    public int minimumSpacing;
    public int maximumSpacing;
}