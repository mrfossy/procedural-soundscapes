using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class genertes the Perlin noise that is projected onto my plane mesh

//The procedural generation code came from the first four parts of a procedural generation YouTube tutorial by Sebastian Lague:
//https://www.youtube.com/watch?v=wbpMiKiSKm8&list=RDCMUCmtyQOKKmrMVaKuRXz02jbQ&index=4
public static class PerlinNoise
{
    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistence, float lacunarity, Vector2 offset)
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        //Pseudo-random number generator
        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];

        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetY = prng.Next(-100000, 100000) + offset.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);
        }

        //Prevent divide by zero error
        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        //Create variables for storing min and max noise height values for conersion later
        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        //Variables for allowing texture to scale up from the centre of the map (not the top right)
        float halfWidth = mapWidth / 2f;
        float halfHeight = mapHeight / 2f;

        //Loop through the map dimensions
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;
                

                //Loop thorugh all of the octaves
                for (int i = 0; i < octaves; i++)
                {
                    //The higher the frequency, the further apart the sample points will be
                    //Generated based on a seed
                    //Adjusted so that the noise map scales around the centre of the texture (not top rght)
                    float sampleX = (x - halfWidth) / scale * frequency + octaveOffsets[i].x;
                    float sampleY = (y - halfHeight) / scale * frequency + octaveOffsets[i].y;

                    //By default, the value returned by Mathf.PerlinNoise is in the range 0-1
                    //In order to get more varied noise, allow for negative Perlin values (hence *2 - 1)
                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    //Increase noise height by the Perlin value of each octave
                    noiseHeight += perlinValue * amplitude;
                    //noiseMap[x, y] = perlinValue;
                    amplitude *= persistence;   //persistence is in range 0-1, and decreases with each octave
                    frequency *= lacunarity;    //frequency increases each octave (since lacunarity is > 1)
                }

                //Clamp the max and min noise heights to the current noise height
                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }            
                 
                //Apply noise height to noise map
                noiseMap[x, y] = noiseHeight;
            }
        }

        //Now you know what range the noise map values are in,
        //loop through noise map values again and normalise the noise map
        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                //For each value in the noise map, clamp the min value to 0, clamp max to 1 (halfway between the two owuld be 0.5)
                //(Mathf.InverseLerp returns a value between 0 and 1)
                noiseMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[x, y]);
            }
        }

        return noiseMap;
    }
}