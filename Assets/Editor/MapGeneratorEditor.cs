using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


//Handles the editor side aspects of the Perlin noise tool
[CustomEditor (typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //target means the object this custom editor is inspecting
        MapGenerator mapGen = (MapGenerator)target;

        //If any value is changed in the inspector, update the texture map
        if (DrawDefaultInspector())
        {
            if (mapGen.autoUpdate)
            {
                mapGen.GenerateMap();
            }
        }

        //Create a new button to generate noise maps
        if (GUILayout.Button("Generate"))
        {
            mapGen.GenerateMap();
        }
    }
}
