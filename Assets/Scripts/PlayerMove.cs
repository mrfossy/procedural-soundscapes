using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class handles all aspects of player movement. I built it on top of the character controller
//shown in the 'Kinematics and Player Controllers' tutorial.
public class PlayerMove : MonoBehaviour
{
    Vector2 moveInput = new Vector2();
    private CharacterController controller;
    [SerializeField] private float moveSpeed = 12.0f;
    private Vector3 velocity;
    [SerializeField] private float gravity = -9.81f;
    bool jumpInput = false;
    [SerializeField] float jumpHeight = 10.0f;
    [SerializeField] float playerMass = 1.0f;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private float groundDistance = 0.4f;
    [SerializeField] private LayerMask groundMask;
    private bool isGrounded;
    //[SerializeField] private SoundscapeCreator soundscapeCreator;
    [SerializeField] private GameObject mainMenu;
    private AudioSource footstepSound;
    [SerializeField] private AudioClip[] footstepSounds;
    private bool menuOpen = false;
    private bool isWalking = false;
    //private AudioSource jumpSound;

    void Start()
    {
        Application.targetFrameRate = 60;
        controller = GetComponent<CharacterController>();
        //soundscapeCreator.GetComponent<SoundscapeCreator>();
        footstepSound = GetComponent<AudioSource>();
        //jumpSound = GetComponent<AudioSource>();
        mainMenu.SetActive(false);
    }

    //Get mouse and keyboard inputs
    void Update()
    {
        moveInput.x = Input.GetAxis("Horizontal");
        moveInput.y = Input.GetAxis("Vertical");
        jumpInput = Input.GetButton("Jump");

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!menuOpen)
            {
                isWalking = false;
                mainMenu.SetActive(true);
                menuOpen = true;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
                mainMenu.SetActive(false);
                menuOpen = false;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            
        }

        //Cycle footstep sounds when the player is walking
        if (isWalking && !footstepSound.isPlaying && isGrounded)
        {
            int totalStepSounds = footstepSounds.Length;
            footstepSound.clip = footstepSounds[Random.Range(0, totalStepSounds)];
            footstepSound.Play();
        }
    }

    //Apply forces to the player's velocity
    private void FixedUpdate()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        //Player movement using WASD or arrow keys
        Vector3 delta = (moveInput.x * transform.right + moveInput.y * transform.forward) * moveSpeed;

        //Modify horizontal values for velocity (i.e. x, z, not y) only if player is grounded, or if input is received
        if (isGrounded || moveInput.x != 0 || moveInput.y != 0)
        {
            velocity.x = delta.x;
            velocity.z = delta.z;
        }

        if (delta != Vector3.zero)
        {
            isWalking = true;
        }
        else
        {
            isWalking = false;
        }

        //Check for jumping
        if (jumpInput && isGrounded)
        {
            /*
            if (!jumpSound.isPlaying)
            {
                jumpSound.Play();

            }
            */

            //Apply 4th kinematic formula to calculate the initial velocity required to reach a specified jump height
            velocity.y = Mathf.Sqrt(-2.0f * gravity * playerMass * jumpHeight);
        }

        //Check if player has hit ground from falling; if so, remove y velocity
        if (isGrounded && velocity.y < 0)
        {
            //Set to -2.0 as opposed to 0.0f just forces the player onto the ground a bit more solidly
            velocity.y = -2.0f;
        }

        //Apply gravity after zeroing velocity so we register as grounded still
        velocity.y += gravity * playerMass * Time.fixedDeltaTime;

        //Apply this to our positional update this frame
        controller.Move(velocity * Time.deltaTime);
    }

    public void AddToVelocity(Vector3 velocity)
    {
        this.velocity += velocity;
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}